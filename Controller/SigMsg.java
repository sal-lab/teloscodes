public class SigMsg extends net.tinyos.message.Message{
	public static final int DEFAULT_MESSAGE_SIZE = 2;

	public static final int AM_TYPE=143;

	public SigMsg(){
		super(DEFAULT_MESSAGE_SIZE);
		amTypeSet(AM_TYPE);
	}

	public static int offset_node(){
		return (0/8);
	}

	public static int offsetBits_node(){
		return 0;
	}

	public int get_node(){
		return (int)getUIntBEElement(offsetBits_node(),8);
	}

	public void set_node(int value){
		setUIntBEElement(offsetBits_node(),8,value);
	}

	public static int offset_value(){
		return (8/8);
	}

	public static int offsetBits_value(){
		return 8;
	}

	public int get_value(){
		return (int)getUIntBEElement(offsetBits_value(),8);
	}

	public void set_value(int value){
		setUIntBEElement(offsetBits_value(),8,value);
	}

}
