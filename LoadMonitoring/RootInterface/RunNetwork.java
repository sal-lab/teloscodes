import java.io.IOException;
import java.sql.*;

import net.tinyos.message.*;
import net.tinyos.packet.*;
import net.tinyos.util.*;

public class RunNetwork implements MessageListener{

	private static MoteIF moteIF;

	static final String driver = "com.mysql.jdbc.Driver";
	static final String url = "jdbc:mysql://localhost/helios-db";

	static final String username = "helios";
	static final String password = "apollo";

	static final int GENERATE_TYPE1_CS = 11;
	static final int GENERATE_TYPE2_CS = 22;
	static final int DO_NOTHING = 33;
	static final int TIVA_ACK = 78;

	static volatile int count = 0;
	static volatile int entry = 0;
	static volatile int curtailCount = -1;
	static volatile int uniqueId = 0;
	static volatile int nodechecker[]= new int[5];

	public RunNetwork(MoteIF moteIF){
		this.moteIF = moteIF;
		this.moteIF.registerListener(new DataSigMsg(), this);
	}

	private static int assignId(){
		if(uniqueId < 255){
			uniqueId = uniqueId + 1;
			return uniqueId;
		}
		else{
			return 0;
		}
	}

	private static int processSelect(){
		Connection conn = null;
		Statement stmt = null;
		int process = 0;

		try{
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url, username, password);
			stmt = conn.createStatement();
			String query = "SELECT * FROM system_stat ORDER BY entryId DESC LIMIT 1";
			ResultSet rs = stmt.executeQuery(query);
			if(rs.next()){
				entry = rs.getInt(1);
				if(rs.getInt(4)==1){ //if new data request bit is set
					process = GENERATE_TYPE1_CS;
				}
				else if(rs.getInt(6) == 1){ //if new curtailment available bit is set
					process = GENERATE_TYPE2_CS;
				}
				else{
					process = DO_NOTHING;
				}
			}
		}
		catch(SQLException e){
			e.printStackTrace();
			process = DO_NOTHING;
		}
		catch(Exception e){
			e.printStackTrace();
			process = DO_NOTHING;
		}
		finally{
			try{
				if(stmt != null)
					conn.close();
			}
			catch(Exception e){
				e.printStackTrace();
			}
			try{
				if(conn != null)
					conn.close();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		return process;
	}

	public static void sendType1(){
		CtrlSigMsg msg = new CtrlSigMsg();
		int newId = assignId();

		msg.set_id(newId);
		msg.set_type(msg.DATA_REQ);
		msg.set_value(0);

		try{
			System.out.println("Sending type 1 control signal to "+ Long.toString(countLoads()-1) +" load(s) and supply: requesting data (" + Long.toString(msg.get_type()) + ") id(" + Long.toString(msg.get_id()) + ")");
			moteIF.send(0, msg);
			System.out.println("CS 1 sent!");
		}
		catch(Exception e){
			System.out.println("Error sending control signal of type 1");
		}
		finally{}
	}

	public static void sendType2(){
		Connection conn = null;
		Statement stmt = null;
		CtrlSigMsg msg = new CtrlSigMsg();
		int numOfChanges = 0;
		int newId = assignId();

		msg.set_id(newId);
		msg.set_type(msg.LOAD_MNG);

		try{
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url, username, password);
			stmt = conn.createStatement();
			String sql = "SELECT * FROM ctlsig_data ORDER BY logId DESC LIMIT 1 ";
			ResultSet rs = stmt.executeQuery(sql);
			if(rs.next()){
				msg.set_value(rs.getInt(4));
				numOfChanges = rs.getInt(5);
			}
		}
		catch(Exception e){
		}
		finally{
			try{
				if(conn != null)
					conn.close();
			}
			catch(Exception e){}
			try{
				if(stmt != null)
					conn.close();
			}
			catch(Exception e){}
		}

		try{
			System.out.println("Sending type 2 control signal: curtailment request (" + Long.toString(msg.get_type())  + ") with value: " + Long.toString(msg.get_value())+" id: " + Long.toString(msg.get_id()));
			moteIF.send(0,msg);
			System.out.println("CS 2 sent!");
		}
		catch(Exception e){
			System.out.println("Error in sending control signal of type 2");
		}
		finally{}
		curtailCount = numOfChanges;
		mainloop(-1, curtailCount);
	}

	private static void mainloop(int liveCount, int numOfChanges){
		Connection conn = null;
		Statement stmt = null;

		System.out.println("Count :" + Long.toString(count) + " LoadCount: " + Long.toString(liveCount) + " CurtailCount: " + Long.toString(numOfChanges));
		if (count < liveCount){
			return;
		}
		else if(count < numOfChanges){
			return;
		}
		else if((liveCount > 0)&&(count >= liveCount)){
			try{
				Class.forName(driver).newInstance();
				conn = DriverManager.getConnection(url, username, password);
				stmt = conn.createStatement();
				String sql = "INSERT INTO system_stat (entryId, date,time,NDRB,NLDB,NCDB,NLSB) VALUES (NULL, CURDATE(), CURTIME(), 0,1,0,1)";
				stmt.executeUpdate(sql);
			}
			catch(Exception e){
				e.printStackTrace();
			}
			finally{
				try{
					if(stmt!=null)
						conn.close();
				}
				catch(Exception e){
					e.printStackTrace();
				}
				try{
					if(conn!=null)
						conn.close();
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			System.out.println("System stats updated: load data available for demand response!\n*******\n");
			count = 0;
			liveCount = -1;
		}
		else if((numOfChanges >=0)&&(count >= numOfChanges)){
			try{
				Class.forName(driver).newInstance();
				conn = DriverManager.getConnection(url, username, password);
				stmt = conn.createStatement();
				String sql = "INSERT INTO system_stat (entryId, date, time,NDRB,NLDB,NCDB,NLSB) VALUES (NULL, CURDATE(), CURTIME(), 0,1,0,1)";
				stmt.executeUpdate(sql);
			}
			catch(Exception e){
				e.printStackTrace();
			}
			finally{
				try{
					if(stmt!=null)
						conn.close();
				}
				catch(Exception e){
					e.printStackTrace();
				}
				try{
					if(conn!=null)
						conn.close();
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			System.out.println("***NOTIFICATION: System stats updated: load curtailment enacted!***\n\n");
			count = 0;
			numOfChanges = -1;
		}

		System.out.print("Waiting for next process: ");

		for(int i = 0; i < 4; i++){
			nodechecker[i] = 0;
		}

		while(processSelect() == DO_NOTHING);
		switch(processSelect()){
			case GENERATE_TYPE1_CS:
				System.out.println("New data request");
				sendType1();
				break;
			case GENERATE_TYPE2_CS:
				System.out.println("New curtailment available");
				sendType2();
				break;
			default:
				System.out.println("ERROR: Unknown state triggered during process selection!");
				System.exit(1);
		}

	}

	private static int countLoads(){
		Connection conn = null;
		Statement stmt = null;
		int loadCount = 0;

		try{
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url, username, password);
			stmt = conn.createStatement();
			String sql = "SELECT * FROM loads";
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()){
				loadCount += 1;
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			try{
				if(stmt!=null)
					conn.close();
			}
			catch(Exception e){
				e.printStackTrace();
			}
			try{
				if(conn!=null)
					conn.close();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		return 1; //plus 1 for the supply
	}

	public void messageReceived(int to, Message message){
		Connection conn = null;
		Statement stmt = null;
		DataSigMsg msg = (DataSigMsg)message;
		int ackCheck = msg.get_ack();
		int nodeIndex;

		if(msg.get_node() == 99){
			nodeIndex = 0;
		}
		else{
			nodeIndex = msg.get_node();
		}

		if(nodechecker[nodeIndex] == 0){
			nodechecker[nodeIndex] = 1;
			count += 1;

			if (ackCheck == 0){
				try{
					Class.forName(driver).newInstance();
					conn = DriverManager.getConnection(url, username, password);
					stmt = conn.createStatement();
					String sql = " ";
					if (msg.get_node() == 99){
						System.out.println("\n******\nNew data received from supply node...");
						sql = "INSERT INTO battery_data VALUES (NULL, CURDATE(), CURTIME(), " +
							Double.toString(msg.get_batt()) + "," +
							Double.toString(msg.get_soc()) + ")";
						System.out.println("Battery voltage: "+Double.toString(msg.get_batt())+"; SOC: "+Double.toString(msg.get_soc())+"\n******\n");
					}
					else{
						System.out.println("\n******\nNew data received from node " + Long.toString(msg.get_node()) + "...");
						sql = "INSERT INTO lmcu_data VALUES (NULL, CURDATE(), CURTIME(), " + Long.toString(msg.get_node()) + ", " +
							Double.toString(msg.get_volt()) + "," +
							Double.toString(msg.get_curr()) + "," +
							Double.toString(msg.get_ppwr()) + "," +
							Double.toString(msg.get_qpwr()) + ","+
							Double.toString(msg.get_spwr()) + "," +
							Double.toString(msg.get_freq()) + "," +
							Double.toString(msg.get_ph()) + ")";
						System.out.println("Real power: "+Double.toString(msg.get_ppwr())+"\n******\n");
					}
					stmt.executeUpdate(sql);
				}
				catch(SQLException se){
					se.printStackTrace();
				}
				catch (Exception e){
					e.printStackTrace();
				}
				finally{
					try {
						if (stmt != null)
							conn.close();
					}catch(SQLException se){}
					try{
						if(conn != null)
							conn.close();
					}catch(SQLException se){
						se.printStackTrace();
					}
				}

				System.out.println("***Waiting data request: motes replied = " + Long.toString(count) + "; pending motes = " + Long.toString(countLoads()-count)+"***");
				mainloop(countLoads(), -1);
			}
			else if(ackCheck == TIVA_ACK){
				System.out.println("\n******\nConfirmation received from node "+Long.toString(msg.get_node()));
				System.out.println("***Waiting curtailment acks: curtailed loads' count= " + Long.toString(count) + "; pending to curtail = " + Long.toString(curtailCount-count)+"***");
				mainloop(-1, curtailCount); //localize global variable to prevent non-atomic access
			}
			else{
				System.out.println("***ERROR:ackCheck returned unknown state! (" + Long.toString(ackCheck)+")***");
				System.exit(1);
			}
		}
		else{} //if nodechecker[index] == 1 (message already received from same node previously
	}

	private static void usage(){
		System.err.println("usage: DataProcess [-begin] [<source>]");
	}


	public static void main(String[] args) throws Exception{
		String source = null;
		CtrlSigMsg cs = new CtrlSigMsg();
		int proc = 0, liveCount = 0, ackCount = 0;

		if (args.length == 2){
			if (!args[0].equals("-begin")){
				usage();
				System.exit(1);
			}
			source = args[1];
		}
		else{
			usage();
			System.exit(1);
		}

		PhoenixSource phoenix;

		if (source == null){
			phoenix = BuildSource.makePhoenix(PrintStreamMessenger.err);
		}
		else{
			phoenix = BuildSource.makePhoenix(source, PrintStreamMessenger.err);
		}

		MoteIF mif = new MoteIF(phoenix);
		RunNetwork basemote = new RunNetwork(mif);

		mainloop(-1,-1);
	}
}
