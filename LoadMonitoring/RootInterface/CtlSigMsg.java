

public class CtlSigMsg extends net.tinyos.message.Message {

	/** The default size of this message type (controlsignal) in bytes **/
	public static final int DEFAULT_MESSAGE_SIZE = 4;

	/** The Active Message type associated with this message (no idea)**/
	public static final int AM_TYPE = 143;

	/** Create a new CtlSigMsg of size 5 bytes */
	public CtlSigMsg(){
		super(DEFAULT_MESSAGE_SIZE);
		amTypeSet(AM_TYPE);
	}

	/**
	 * Return a String representation of this message. Includes the
	 * message type name and the non-indexed field values.
	 */
	public String toString(){
		String s = " ";
		try{
			s += "  [ctrl1=0x"+Long.toHexString(get_ctrl1())+",";
			s += " ctrl2=0x"+Long.toHexString(get_ctrl2())+",";
			s += " ctrl3=0x"+Long.toHexString(get_ctrl3())+"]\n";
		}
		catch (ArrayIndexOutOfBoundsException aioobe) {}
		return s;
	}

	public void printPacketString(){
		String s = " ";
		try{
			printHex(get_ctrl1());
			printHex(get_ctrl2());
			printHex(get_ctrl3());
		}
		catch (ArrayIndexOutOfBoundsException aioobe){}
	}

	public void printHex(int b){
		String s = Long.toHexString(b & 0xff).toUpperCase();
		if (b >=0 && b < 16)
			System.out.print("0");
		System.out.print(s + " ");
	}

		////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'reqId'
	//    all fields type: int, unsigned
	//    offset (bits): 0
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////
	  
	 /**
	  * Return the offset(in bytes) of the field 'reqId'
	  */
	public static int offset_req(){
		 return(0 / 8);
	 }
		
	 /**
	  * Return the offset(in bits) of the field 'reqId'
	  */
	 public static int offsetBits_req(){
		 return 0;
	 }
	
	/**
	 * Return the value (as an int) of the field 'reqId'
	 */
	public int get_req(){
		 return (int)getUIntBEElement(offsetBits_req(),8);
	}
		 
	/**
	 * Set the value of the field 'reqId'
	 */
	public void set_req(int value){
		setUIntBEElement(offsetBits_req(),8,value);
	}
	
	/**
	 * return the size (in bytes) of the field 'reqId'
	 */
	public static int size_req(){
		return (8/8);
	}
	
	/**
	 * Return the size (in bits) of the field 'reqId'
	 */
	public static int sizeBits_req(){
		return 8;
	}
	
	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'ctrl1'
	//    all fields type: int, unsigned
	//    offset (bits): 8
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////

	/**
	 * Return whether the fields are signed (false)
	 */
	 public static boolean isSigned_ctl1(){
		 return false;
	 }
	 
	 /**
	  * return whether the fields are arrays (false)
	  */
	 public static boolean isArray_ctl1(){
		 return false;
	 }
	  
	 /**
	  * Return the offset(in bytes) of the field 'ctrl1'
	  */
	 public static int offset_ctrl1(){
		 return(8 / 8);
	 }
		
	 /**
	  * Return the offset(in bits) of the field 'ctrl1'
	  */
	 public static int offsetBits_ctrl1(){
		 return 8;
	 }
	
	/**
	 * Return the value (as an int) of the field 'ctrl1'
	 */
	public int get_ctrl1(){
		 return (int)getUIntBEElement(offsetBits_ctrl1(),8);
	}
		 
	/**
	 * Set the value of the field 'ctrl1'
	 */
	public void set_ctrl1(int value){
		setUIntBEElement(offsetBits_ctrl1(),8,value);
	}
	
	/**
	 * return the size (in bytes) of the field 'ctrl1'
	 */
	public static int size_ctrl1(){
		return (8/8);
	}
	
	/**
	 * Return the size (in bits) of the field 'ctrl1'
	 */
	public static int sizeBits_ctrl1(){
		return 8;
	}
	
	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'ctrl2'
	//    all fields type: int, unsigned
	//    offset (bits): 16
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////
	
	/**
	 * Return whether the fields are signed (false)
	 */
	 public static boolean isSigned_ctrl2(){
		 return false;
	 }
	 
	 /**
	  * return whether the fields are arrays (false)
	  */
	 public static boolean isArray_ctrl2(){
		 return false;
	 }
	  
	 /**
	  * Return the offset(in bytes) of the field 'ctrl2'
	  */
	 public static int offset_ctrl2(){
		 return(16 / 8);
	 }
		
	 /**
	  * Return the offset(in bits) of the field 'ctrl2'
	  */
	 public static int offsetBits_ctrl2(){
		 return 16;
	 }
	
	/**
	 * Return the value (as an int) of the field 'ctrl2'
	 */
	public int get_ctrl2(){
		 return (int)getUIntBEElement(offsetBits_ctrl2(),8);
	}
		 
	/**
	 * Set the value of the field 'ctrl2'
	 */
	public void set_ctrl2(int value){
		setUIntBEElement(offsetBits_ctrl2(),8,value);
	}
	
	/**
	 * return the size (in bytes) of the field 'ctrl2'
	 */
	public static int size_ctrl2(){
		return (8/8);
	}
	
	/**
	 * Return the size (in bits) of the field 'ctrl2'
	 */
	public static int sizeBits_ctrl2(){
		return 8;
	}
	
	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'ctrl3'
	//    all fields type: int, unsigned
	//    offset (bits): 24
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////
	
	/**
	 * Return whether the fields are signed (false)
	 */
	 public static boolean isSigned_ctrl3(){
		 return false;
	 }
	 
	 /**
	  * return whether the fields are arrays (false)
	  */
	 public static boolean isArray_ctrl3(){
		 return false;
	 }
	  
	 /**
	  * Return the offset(in bytes) of the field 'ctrl3'
	  */
	 public static int offset_ctrl3(){
		 return(24 / 8);
	 }
		
	 /**
	  * Return the offset(in bits) of the field 'ctrl3'
	  */
	 public static int offsetBits_ctrl3(){
		 return 24;
	 }
	
	/**
	 * Return the value (as an int) of the field 'ctrl3'
	 */
	public int get_ctrl3(){
		 return (int)getUIntBEElement(offsetBits_ctrl3(),8);
	}
		 
	/**
	 * Set the value of the field 'ctrl3'
	 */
	public void set_ctrl3(int value){
		setUIntBEElement(offsetBits_ctrl3(),8,value);
	}
	
	/**
	 * return the size (in bytes) of the field 'ctrl3'
	 */
	public static int size_ctrl3(){
		return (8/8);
	}
	
	/**
	 * Return the size (in bits) of the field 'ctrl3'
	 */
	public static int sizeBits_ctrl3(){
		return 8;
	}
}
