module testIntC {
	uses{
		interface HplMsp430Interrupt as IntPin; //pin 3, port 23, GIO2
		interface Boot;
		interface Leds;
		interface HplMsp430GeneralIO as Pin; //pin 4, port 26, GIO3
		interface Timer<TMilli> as Clk;
		interface Timer<TMilli> as Clk1;
	}
}

implementation{
	event void Boot.booted(){
		call Pin.makeOutput();
		call Clk.startPeriodic(2048);
		call IntPin.edge(TRUE);
		call IntPin.enable();
	}
	
	async event void IntPin.fired(){
		call IntPin.clear();
		call Leds.led0Toggle();
	}

	event void Clk.fired(){
		call Pin.set();
		call Clk1.startOneShot(500);
	}
	
	event void Clk1.fired(){
		call Pin.clr();
		call Leds.led1Toggle();
	}
}
