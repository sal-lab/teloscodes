configuration testIntAppC{
}

implementation{
	components testIntC as App;
	components MainC, LedsC;
	components HplMsp430InterruptC;
	components HplMsp430GeneralIOC;
	components new TimerMilliC() as Timer1;
	components new TimerMilliC() as Timer2;
	App.Boot -> MainC;
	App.Leds -> LedsC;
	App.IntPin -> HplMsp430InterruptC.Port23;
	App.Pin -> HplMsp430GeneralIOC.Port26;
	App.Clk -> Timer1;
	App.Clk1 -> Timer2;
}
