
#ifndef DATASTRUCT_H
#define DATASTRUCT_H

#define NORMAL			0
#define ARDUINO_TO_TELOS 	1
#define TELOS_TO_RPI		2
#define ARDUINO_TO_RPI		3
#define INITIAL_STATE		4
#define RPI_TO_ARDUINO		5
#define RPI_TO_TELOS		6
#define TELOS_TO_ARDUINO	7

enum{
	AM_WSN=6
};

typedef nx_struct CtlSig_msg{
	nx_uint8_t node_id;
	nx_uint8_t status;
}CtlSig_t;

#endif
