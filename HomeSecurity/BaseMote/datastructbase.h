
#ifndef DATASTRUCT_H
#define DATASTRUCT_H

enum{
	AM_WSN=6,
	SERIAL_ID=143,
	UART_DATALEN=2 //5-bit binary data (int 0 - int 31)
};

typedef nx_struct CtlSig_msg{
	nx_uint8_t node_id;
	nx_uint8_t status;
}CtlSig_t;

#endif