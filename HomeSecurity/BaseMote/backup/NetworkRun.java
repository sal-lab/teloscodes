
import java.io.IOException;
import java.sql.*;

import net.tinyos.message.*;
import net.tinyos.packet.*;
import net.tinyos.util.*;

public class NetworkRun implements MessageListener{

	private MoteIF moteIF;  //mote handler/source?
	private static int packetCount = 0;

	static final String driver = "com.mysql.jdbc.Driver";
	static final String dbUrl = "jdbc:mysql://localhost/wsn-db";

	static final String username = "JEN";
	static final String password = "1209jen3487";
	private static int siren;

	public NetworkRun(MoteIF moteIF){ //instance constructor
		this.moteIF = moteIF; //setting instance parameter (1): mote source
		this.moteIF.registerListener(new CtlSigMsg(), this); //parameter (2): packet format to listen to and from what source
	}

	public void updateNetwork(){
		CtlSigMsg payload = new CtlSigMsg();
		Connection conn = null; //mysql connection handler
		Statement stmt = null; //mysql query handler

		try{
			Class.forName(driver).newInstance(); //create new instance of database connector
			conn = DriverManager.getConnection(dbUrl, username, password); //create connection to database
			stmt = conn.createStatement(); //create query handler instance
			String query = "SELECT * FROM sensor_data ORDER BY logID DESC LIMIT 1";
			ResultSet rs = stmt.executeQuery(query);
			if (rs.next()){
				payload.set_nodeID(0); //set source of packet as basemote
				int sig = rs.getInt(2)*32 + rs.getInt(3)*16 + rs.getInt(4)*8 + rs.getInt(5)*4 + rs.getInt(6)*2;
				payload.set_status(sig & 255); //copy intrusion status to ctl signal
				try{
					moteIF.send(0,payload);
				}
				catch(IOException ex){
					System.err.println("Error in sending packet to telos: " + ex);
				}
			}
		}
		catch(SQLException se){
			System.err.println("Error in updatenetwork function");
			se.printStackTrace();
		}
		catch (Exception e){
			e.printStackTrace();
		}
		finally{
			try{
				if(stmt != null)
					conn.close();
			}
			catch(SQLException se){
				se.printStackTrace();
			}
		}
	}

	public void messageReceived(int to, Message message){
		Connection conn = null;
		Statement stmt = null;
		CtlSigMsg msg = (CtlSigMsg)message;
		int mote = msg.get_nodeID();
		int data = msg.get_status();
		String sql = " ";
		siren = 0;
		packetCount++;
		try{
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(dbUrl, username, password);
			stmt = conn.createStatement();
			sql = "UPDATE sensor_data SET date=CURDATE(), time=CURTIME()";
			switch(mote){
				case 1: //doorbell
				sql += ", sensor1=" + Long.toString((data & (1<<1))>>1);
				break;
				case 2: //laser detector
				sql += ", sensor2=" + Long.toString((data & (1<<2))>>2);
				if (((data & (1<<2))>>2)==1){
					siren = 1;
					sql+= ", sensor3=1";
				}
				else
					siren = 0;
				break;
				case 3: //siren
				sql += ", sensor3=" + Long.toString((data & (1<<3))>>3);
				break;
				case 4: //doorlock
				sql += ", sensor4=" + Long.toString((data & (1<<4))>>4);
				break;
				case 5: //laser source
				sql += ", sensor5=" + Long.toString((data & (1<<5))>>5);
				break;
				default:
			}
			sql += " WHERE logID=1";
			stmt.executeUpdate(sql);
		}
		catch(SQLException se){
			se.printStackTrace();
		}
		catch (Exception e){
			e.printStackTrace();
		}
		finally{
			try {
				if (stmt != null)
					conn.close();
			}catch(SQLException se){
			}
			try{
				if(conn != null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}
		}

		msg.printPacketString();
		System.out.println(sql);
		msg.set_nodeID(0);
		msg.set_status(siren*8);
		try{
			System.out.println("Sending packet");
			moteIF.send(0, msg);
			System.out.println("Packet sent!");
		}
		catch(IOException ioe){
			System.out.println("Error sending ctrl signal");
		}

	}

	private static void usage(){
		System.err.println("Usage: NetworkRun [-begin] [<source>]");
	}

	public static void main(String[] args) throws Exception{
		String source = null;

		if (args.length == 2){
			if (!args[0].equals("-begin")){
				usage();
				System.exit(1);
			}
			else{
				source = args[1];
			}
		}
		else if(args.length != 0){
			usage();
			System.exit(1);
		}

		PhoenixSource phoenix;

		if (source == null){
			phoenix = BuildSource.makePhoenix(PrintStreamMessenger.err);
		}
		else{
			phoenix = BuildSource.makePhoenix(source, PrintStreamMessenger.err);
		}


		MoteIF mif = new MoteIF(phoenix);
		NetworkRun basemote = new NetworkRun(mif);
	}

}
