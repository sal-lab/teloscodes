/*
 *
 * Implementation of Base Station Interface application
 *
 * @author: Mark Anthony Cabilo
 * @date  : 
 * @revised: 
 */
 
#include "Timer.h"
#include "datastruct.h"

module RootInterfaceC {
	uses{
		interface Boot;
#ifdef DEBUG
		interface Leds;
#endif
		
		interface SplitControl as SerialControl;
		interface SplitControl as RadioControl;
		
		/**radio interface**/
		//collection
		interface StdControl as CollectionControl;
		interface RootControl;
		interface Receive as CollectionReceive[am_id_t id];
		interface Packet as RadioPacket;
		interface CollectionPacket;
		
		//dissemination
		interface DisseminationUpdate<controlmsg_t> as SignalUpdate;
		interface StdControl as DisseminationControl;
		
		/**serial interface**/
		interface Receive as UartReceive;
		interface AMSend as UartSend;
		interface Packet as UartPacket;
		interface AMPacket as UartAMPacket;
		
		interface Queue<message_t *>;
		interface Pool <message_t>;
	}
}

implementation{
	
	message_t packet;
	bool fwdBusy = FALSE;
	message_t fwdMsg;
	message_t uartmsg;
	
	/********************** TASKS AND FUNCTIONS ************************/
	/* -----------------------------------------------------------------
		task definition for sending WSN data to the serial
			* prints data to the java program in RPi
	----------------------------------------------------------------- */
	task void serialForwardTask() {
		if (!call Queue.empty() && !fwdBusy) {
			message_t* msg = call Queue.dequeue();
			uint8_t len = call RadioPacket.payloadLength(msg);
			void *radio_payload = call RadioPacket.getPayload(msg, len);
			am_addr_t src = call CollectionPacket.getOrigin(msg);

			if (radio_payload != NULL) {
				void *uart_payload;
				memcpy(&uartmsg, radio_payload, len);

				call UartPacket.clear(msg);
				call UartAMPacket.setSource(msg, src);
				uart_payload = call UartPacket.getPayload(msg, len);
				if (uart_payload != NULL) {
					memcpy(uart_payload, &uartmsg, len);

					if (call UartSend.send(AM_BROADCAST_ADDR, msg, len) == SUCCESS) {
						fwdBusy = TRUE;
					}
				}
			}
		}
	}
	
	/*************************** EVENTS *******************************/
	/* -----------------------------------------------------------------
		PROGRAM STARTS HERE: device completes booting up
	----------------------------------------------------------------- */
	event void Boot.booted(){
		call SerialControl.start();
		call RadioControl.start();
	}
	
	/* -----------------------------------------------------------------
		SERIAL HARDWARE is ready for control
			* what to do when SerialControl finished initialization
	----------------------------------------------------------------- */
	event void SerialControl.startDone(error_t err){
		fwdBusy = FALSE;
			
		if (err==SUCCESS){
			if(!call Queue.empty())
				post serialForwardTask();
		}
		else
			call SerialControl.start();
	}
	
	/* -----------------------------------------------------------------
		split-control event completed (callback)
			* what to do when SerialControl finished stopping
	----------------------------------------------------------------- */
	event void SerialControl.stopDone(error_t err){}
	
	/* -----------------------------------------------------------------
		RADIO HARDWARE is ready for control
			* what to do when SerialControl finished initialization
	----------------------------------------------------------------- */
	event void RadioControl.startDone(error_t err){
		if (err==FAIL){
			call RadioControl.start();
		}
		else {
			call CollectionControl.start();
			call DisseminationControl.start();
			call RootControl.setRoot();
		}
	}
	
	/* -----------------------------------------------------------------
		split-control event completed (callback)
			* what to do when RadioControl finished stopping
	----------------------------------------------------------------- */
	event void RadioControl.stopDone(error_t err) {}
	
	/* -----------------------------------------------------------------
		BASE STATION MOTE RECEIVES A PACKET FROM OTHER MOTES:
			* what to do when mote receives packet from CTP
	------------------------------------------------------------------*/
	event message_t* CollectionReceive.receive[collection_id_t id] 
	(message_t* msg, void* payload, uint8_t len){
		if (!call Pool.empty() && call Queue.size() < call Queue.maxSize()) {
			message_t *tmp = call Pool.get();
			call Queue.enqueue(msg);
			if (!fwdBusy) {
				post serialForwardTask();
			}	
			return tmp;
		}	
		return msg;
	}
	
	/* -----------------------------------------------------------------
		BASE STATION MOTE RECEIVES A PACKET FROM SERIAL:
			* what to do when mote receives packet from RPi
	------------------------------------------------------------------*/
	event message_t* UartReceive.receive (message_t* msg, void* payload, uint8_t len){
		if (len != sizeof(controlmsg_t)){
#ifdef DEBUG
			call Leds.led0Toggle();
#endif			
			return msg;
		}
		else{
			controlmsg_t* rcm = (controlmsg_t*)payload;
			
			call SignalUpdate.change(rcm);
#ifdef DEBUG
			call Leds.led1Toggle();
#endif
			return msg;
		}
	}
	
	/* -----------------------------------------------------------------
		SENDING DATA THROUGH UART:
			* Serial Forwarding Task finished sending a packet
			to the RPi
	----------------------------------------------------------------- */
	event void UartSend.sendDone(message_t* msg, error_t err){
		fwdBusy = FALSE;
		call Pool.put(msg);
		if (! call Queue.empty())
			post serialForwardTask();
	}
}/** end of implementation **/
